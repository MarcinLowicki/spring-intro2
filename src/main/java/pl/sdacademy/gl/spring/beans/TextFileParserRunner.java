package pl.sdacademy.gl.spring.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class TextFileParserRunner {
//plik i parser

    @Value("classpath:files/file.csv")
    private Resource file;

    @Autowired
    private TextFileParser textFileParser;

    public void run(){
        this.textFileParser.parse(this.file);
    }


}
