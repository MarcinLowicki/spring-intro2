package pl.sdacademy.gl.spring.beans;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

@Configuration
@PropertySource("classpath:application.properties")
public class FileLineValidatorConfiguration {

    @Getter
    @Value("#{ '${correct.words}'.split(',') }")
    private List<String> correctWords;

}
