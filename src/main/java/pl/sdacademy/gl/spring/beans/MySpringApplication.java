package pl.sdacademy.gl.spring.beans;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MySpringApplication {

    public static void main(String[] args) {
        //BeanFactory
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("pL.sdacademy.gl.spring");

        TextFileParserRunner runner = context.getBean(TextFileParserRunner.class);
        runner.run();
    }


}
