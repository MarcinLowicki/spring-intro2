package pl.sdacademy.gl.spring.beans;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileLineValidatorTest {
    //test jednostkowy bez springa z mokito. Mickito robi tu proxy.

    private FileLineValidatorConfiguration configuration;
    private FileLineValidator validator;

    @BeforeEach
    void setUp() {
        configuration = Mockito.mock(FileLineValidatorConfiguration.class);
        Mockito.when(configuration.getCorrectWords()).thenReturn(List.of("abc"));
        validator = new FileLineValidator(configuration);
    }

    @Test
    void shouldValidateCorrectly() {
        //given - null

        // when
        var result = this.validator.validate("An Audi is a very super car.");
        //then
        assertEquals(result, true);
        assertTrue(result);
    }

    @Test
    void shouldValidateIncorrectly() {

        //when
        var result = this.validator.validate("BMW is a fine car too.");
        //then
        assertFalse(result);
    }
}